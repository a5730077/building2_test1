from django.conf.urls import url

from . import views

app_name = 'cal'
urlpatterns = [
    url(r'^$',views.home_page , name='home_page'),
    url(r'^calculator/$',views.calculator, name='calculator')

]


