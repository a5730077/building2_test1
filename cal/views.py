from django.shortcuts import render

# Create your views here.
def home_page(request):
    return render(request,'home.html')

def calculator(request):
    a = float(request.GET['number a'])
    b = float(request.GET['number b'])
    act = request.GET['action']
    context = {'a':a,'b':b,'act':act}
    if act =='+':
        context['result'] = a+b
    elif act == '-':
        context['result'] = a-b
    elif act == '*':
        context['result'] = a*b
    else:
        context['result'] = a/b
    return render(request,'calculator.html',context)